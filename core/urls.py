# core/urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('thread/<int:id>/', views.thread_detail, name='thread_detail'),
    path('thread/new/', views.create_thread, name='create_thread'),
    path('my_threads/', views.my_core_list, name='my_core_list'),
    path('delete_thread/<int:id>/', views.delete_thread, name='delete_thread'),
    path('delete_comment/<int:id>/', views.delete_comment, name='delete_comment'),
]
