from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from .forms import SignUpForm, LoginForm
from django.contrib.auth.forms import UserCreationForm
from django.db import IntegrityError

def user_logout(request):
    logout(request)
    return redirect('index')


def user_login(request):
    if request.method == "POST":
        print("POST request received")
        print(request.POST)  # Debugging line to print form data

        username = request.POST.get('username')
        password = request.POST.get('password')
        print(f"Username: {username}, Password: {password}")  # Debugging line

        user = authenticate(request, username=username, password=password)
        if user is not None:
            print("User authenticated")
            login(request, user)
            return redirect('index')
        else:
            print("Authentication failed")
            form = LoginForm(request.POST)
            form.add_error(None, "Invalid username or password")
    else:
        print("GET request received")
        form = LoginForm()
    return render(request, "accounts/login.html", {'form': form})
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('login')
            except IntegrityError:
                form.add_error('username', 'This username is already taken.')
    else:
        form = UserCreationForm()
    return render(request, 'accounts/signup.html', {'form': form})
