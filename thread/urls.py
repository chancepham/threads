# core/urls.py

from django.urls import path,include
from core.views import index,thread_detail,create_thread
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', index, name='index'),
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
    path('thread/<int:id>/', thread_detail, name='thread_detail'),
    path('thread/new/', create_thread, name='create_thread'),
    path('accounts/', include('accounts.urls')),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
